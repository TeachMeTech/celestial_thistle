var projectName = 'Celestial Thistle';
var versionNumber = '0.3.0';
var currentDate = new Date(); //Date Object

//currentTime will look like '2017-11-day- at 0H:0M:0S'

var currentTime = currentDate.getFullYear() + '-' +  //Set Year
				  currentDate.getMonth() + '-' +     //Set Month
				  currentDate.getDate() + 'at' +     //Set Day
				  currentDate.getHours() + 'H:' +    //Set Hours
				  currentDate.getMinutes() + 'M:' +  //Set Minutes
				  currentDate.getSeconds() + 'S:' +  //Set Seconds 